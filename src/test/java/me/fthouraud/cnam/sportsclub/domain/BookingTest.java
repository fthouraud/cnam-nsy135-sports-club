package me.fthouraud.cnam.sportsclub.domain;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class BookingTest {

    private Booking booking;

    @Before
    public void setUp() throws Exception {
        this.booking = new Booking();
    }

    @Test
    public void isFromLastMonth_should_return_false_when_booked_for_same_month() {
        final LocalDateTime bookedFor = LocalDateTime.now();

        booking.setBookedFor(bookedFor);

        assertFalse(booking.isFromLastMonth());
    }

    @Test
    public void isFromLastMonth_should_return_false_when_booked_for_before_last_month() {
        final LocalDateTime bookedFor = LocalDateTime.now().minusMonths(2);

        booking.setBookedFor(bookedFor);

        assertFalse(booking.isFromLastMonth());
    }

    @Test
    public void isFromLastMonth_should_return_true_when_booked_for_same_month() {
        final LocalDateTime bookedFor = LocalDateTime.now().minusMonths(1);

        booking.setBookedFor(bookedFor);

        assertTrue(booking.isFromLastMonth());
    }

}
