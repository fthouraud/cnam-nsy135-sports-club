package me.fthouraud.cnam.sportsclub.domain;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

public class SubscriptionTest {

    private Subscription subscription;

    @Before
    public void setUp() {
        subscription = new Subscription();
    }

    @Test
    public void isActive_should_return_true_when_less_than_a_year() {
        subscription.setSubscribedOn(LocalDateTime.now().minusYears(1).plusMinutes(1));

        Assert.assertTrue(subscription.isActive());
    }

    @Test
    public void isActive_should_return_false_when_more_than_a_year() {
        subscription.setSubscribedOn(LocalDateTime.now().minusYears(1).minusMinutes(1));

        Assert.assertFalse(subscription.isActive());
    }

    @Test
    public void getMonthlyAmount_should_return_zero_when_null() {
        Assert.assertEquals(BigDecimal.ZERO, subscription.getMonthlyAmount());
    }

    @Test
    public void getMonthlyAmount_should_return_a_twelve_divided_amount() {
        subscription.setAmount(BigDecimal.valueOf(12000));

        Assert.assertEquals(BigDecimal.valueOf(1000), subscription.getMonthlyAmount());
    }

    @Test
    public void getPriceByCourtType_should_return_of_price_of_correct_type() {
        final ApplicablePrice ap1 = new ApplicablePrice();
        ap1.setCourtType(CourtType.BADMINTON);
        ap1.setPrice(BigDecimal.ONE);

        final ApplicablePrice ap2 = new ApplicablePrice();
        ap2.setCourtType(CourtType.TENNIS);
        ap2.setPrice(BigDecimal.ZERO);

        final ApplicablePrice ap3 = new ApplicablePrice();
        ap3.setCourtType(CourtType.SQUASH);
        ap3.setPrice(BigDecimal.TEN);

        subscription.setApplicablePrices(Arrays.asList(ap1, ap2, ap3));

        Assert.assertEquals(BigDecimal.ONE, subscription.getPriceByCourtType(CourtType.BADMINTON));
    }

    @Test
    public void getPriceByCourtType_should_return_zero_when_type_is_missing() {
        final ApplicablePrice ap1 = new ApplicablePrice();
        ap1.setCourtType(CourtType.BADMINTON);
        ap1.setPrice(BigDecimal.ONE);

        final ApplicablePrice ap2 = new ApplicablePrice();
        ap2.setCourtType(CourtType.TENNIS);
        ap2.setPrice(BigDecimal.ZERO);

        subscription.setApplicablePrices(Arrays.asList(ap1, ap2));

        Assert.assertEquals(BigDecimal.ZERO, subscription.getPriceByCourtType(CourtType.SQUASH));
    }

}
