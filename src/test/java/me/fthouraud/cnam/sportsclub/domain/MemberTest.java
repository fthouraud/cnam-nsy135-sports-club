package me.fthouraud.cnam.sportsclub.domain;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MemberTest {

    private Member member;

    @Before
    public void setUp() {
        this.member = new Member();
    }

    @Test
    public void getActiveSubscription_should_return_active_subscription() {
        final Subscription s1 = mock(Subscription.class);
        final Subscription active = mock(Subscription.class);
        final Subscription s2 = mock(Subscription.class);

        doReturn(Boolean.FALSE).when(s1).isActive();
        doReturn(Boolean.TRUE).when(active).isActive();
        doReturn(Boolean.FALSE).when(s2).isActive();

        member.setSubscriptions(Arrays.asList(s1, active, s2));

        final Optional<Subscription> optionalSubscription = member.getActiveSubscription();
        assertTrue(optionalSubscription.isPresent());
        assertEquals(active, optionalSubscription.get());
    }

    @Test
    public void getActiveSubscription_should_return_empty_optional() {
        final Subscription s1 = mock(Subscription.class);
        final Subscription s2 = mock(Subscription.class);

        doReturn(Boolean.FALSE).when(s1).isActive();
        doReturn(Boolean.FALSE).when(s2).isActive();

        member.setSubscriptions(Arrays.asList(s1, s2));

        final Optional<Subscription> optionalSubscription = member.getActiveSubscription();
        assertFalse(optionalSubscription.isPresent());
    }

    @Test
    public void getLastSubscription_should_return_last_subscription() {
        final Subscription s1 = new Subscription();
        s1.setSubscribedOn(LocalDateTime.now().minusYears(1));
        final Subscription last = new Subscription();
        last.setSubscribedOn(LocalDateTime.now());
        final Subscription s2 = new Subscription();
        s2.setSubscribedOn(LocalDateTime.now().minusYears(2));

        member.setSubscriptions(Arrays.asList(s1, last, s2));

        final Optional<Subscription> optionalSubscription = member.getLastSubscription();
        assertTrue(optionalSubscription.isPresent());
        assertEquals(last, optionalSubscription.get());
    }

    @Test
    public void getLastSubscription_should_return_empty_optional() {
        final Optional<Subscription> optionalSubscription = member.getLastSubscription();
        assertFalse(optionalSubscription.isPresent());
    }

    @Test
    public void getLastMonthBilledAmount_should_return_amout_of_last_month_only() {
        final Subscription subscription = mock(Subscription.class);
        doReturn(BigDecimal.TEN).when(subscription).getMonthlyAmount();
        doReturn(LocalDateTime.now()).when(subscription).getSubscribedOn();

        final Booking b1 = mock(Booking.class);
        doReturn(Boolean.TRUE).when(b1).isFromLastMonth();
        doReturn(BigDecimal.ONE).when(b1).getBilledAmount();

        final Booking b2 = mock(Booking.class);
        doReturn(Boolean.TRUE).when(b2).isFromLastMonth();
        doReturn(BigDecimal.ONE).when(b2).getBilledAmount();

        final Booking b3 = mock(Booking.class);
        doReturn(Boolean.TRUE).when(b3).isFromLastMonth();
        doReturn(BigDecimal.ONE).when(b3).getBilledAmount();

        final Booking notFromLastMonth = mock(Booking.class);
        doReturn(Boolean.FALSE).when(notFromLastMonth).isFromLastMonth();
        doReturn(BigDecimal.ONE).when(notFromLastMonth).getBilledAmount();

        member.setSubscriptions(Collections.singletonList(subscription));
        member.setBookings(Arrays.asList(b1, b2, b3, notFromLastMonth));

        final BigDecimal lastMonthBilledAmount = member.getLastMonthBilledAmount();

        assertEquals(BigDecimal.valueOf(13), lastMonthBilledAmount);
    }

    @Test
    public void getLastMonthBilledAmount_should_return_zero_when_no_booking_for_last_month() {
        final Subscription subscription = mock(Subscription.class);
        doReturn(BigDecimal.TEN).when(subscription).getMonthlyAmount();

        final Booking b1 = mock(Booking.class);
        doReturn(Boolean.FALSE).when(b1).isFromLastMonth();
        doReturn(BigDecimal.ONE).when(b1).getBilledAmount();

        final Booking b2 = mock(Booking.class);
        doReturn(Boolean.FALSE).when(b2).isFromLastMonth();
        doReturn(BigDecimal.ONE).when(b2).getBilledAmount();

        final BigDecimal lastMonthBilledAmount = member.getLastMonthBilledAmount();

        assertEquals(BigDecimal.ZERO, lastMonthBilledAmount);
    }

}
