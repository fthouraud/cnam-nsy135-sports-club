package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;

@Getter
@Setter
@Builder
public class FinancialInformationDto {

    private BigDecimal incomeOfTheYear;
    private BigDecimal maintenanceCostOfTheYear;
    private Map<String, BigDecimal> yearlyIncomePerMembers;
    private Map<Integer, Map<Integer, BigDecimal>> yearlyIncomePerMonths;
    private Map<Integer, Map<CourtType, BigDecimal>> yearlyIncomePerCourtTypes;
    private Map<Integer, Map<SubscriptionType, Long>> yearlySubscriptionsBySubscriptionTypes;

}
