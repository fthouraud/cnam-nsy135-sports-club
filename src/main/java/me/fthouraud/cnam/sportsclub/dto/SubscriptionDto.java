package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscriptionDto {

    private Long id;
    private SubscriptionType type;
    private String subscribedOn;
    private String amount;

}
