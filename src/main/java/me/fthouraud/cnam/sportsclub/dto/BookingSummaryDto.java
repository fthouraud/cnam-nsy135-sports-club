package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingSummaryDto {

    private Long id;
    private Long memberId;
    private String memberName;
    private Long courtId;
    private CourtType courtType;
    private String bookedForDate;
    private String bookedForTime;
    private boolean cancelled;

}
