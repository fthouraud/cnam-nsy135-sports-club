package me.fthouraud.cnam.sportsclub.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;

import java.util.List;

@Getter
@Setter
public class CourtDetailDto {

    @Delegate
    private CourtSummaryDto courtSummary;
    private List<MaintenanceOperationDto> maintenanceOperations;

}
