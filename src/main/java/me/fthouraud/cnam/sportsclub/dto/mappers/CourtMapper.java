package me.fthouraud.cnam.sportsclub.dto.mappers;

import me.fthouraud.cnam.sportsclub.domain.Court;
import me.fthouraud.cnam.sportsclub.dto.CourtDetailDto;
import me.fthouraud.cnam.sportsclub.dto.CourtSummaryDto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {MaintenanceOperationMapper.class})
public interface CourtMapper {

    CourtSummaryDto toCourtDto(Court court);

    @Mapping(target = "courtSummary", expression = "java( toCourtDto(court) )")
    CourtDetailDto toCourtDetailDto(Court court);

}
