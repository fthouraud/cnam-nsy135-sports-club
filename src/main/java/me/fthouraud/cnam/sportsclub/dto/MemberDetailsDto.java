package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class MemberDetailsDto {

    @Delegate
    private MemberSummaryDto memberSummary;
    private String fullName;
    private String birthDate;
    private BigDecimal lastMonthBilledAmount;
    private Map<CourtType, BigDecimal> applicablePrices;
    private List<SubscriptionDto> subscriptions;
    private List<BookingDto> bookings;

}
