package me.fthouraud.cnam.sportsclub.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FinancialFilterDto {

    private int year;

}
