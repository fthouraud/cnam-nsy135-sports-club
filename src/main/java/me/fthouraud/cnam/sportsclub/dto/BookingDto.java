package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingDto {

    private Long id;
    private String bookedFor;
    private String bookedOn;
    private CourtType courtType;
    private String billedAmount;

}
