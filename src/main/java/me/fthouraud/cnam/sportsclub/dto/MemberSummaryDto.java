package me.fthouraud.cnam.sportsclub.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberSummaryDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private int bookingCount;
    private boolean hasActiveSubscription;

}
