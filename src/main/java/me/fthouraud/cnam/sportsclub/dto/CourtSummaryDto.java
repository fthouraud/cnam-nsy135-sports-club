package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourtSummaryDto {

    private Long id;
    private CourtType type;
    private String commissioningDate;
    private int rating;

}
