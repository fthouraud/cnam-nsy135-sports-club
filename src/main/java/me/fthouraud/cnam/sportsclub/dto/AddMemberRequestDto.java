package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddMemberRequestDto {

    private String firstName;
    private String lastName;
    private String birthDate;
    private String email;
    private SubscriptionType subscriptionType;

}
