package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.OperationType;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class MaintenanceOperationDto {

    private Long id;
    private Long courtId;
    private OperationType type;
    private String description;
    private String maintenanceDate;
    private BigDecimal cost;

}
