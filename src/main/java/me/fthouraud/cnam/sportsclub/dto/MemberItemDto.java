package me.fthouraud.cnam.sportsclub.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItemDto {

    private Long id;
    private String fullName;

}
