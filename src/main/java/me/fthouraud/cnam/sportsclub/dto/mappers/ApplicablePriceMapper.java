package me.fthouraud.cnam.sportsclub.dto.mappers;

import me.fthouraud.cnam.sportsclub.domain.ApplicablePrice;
import me.fthouraud.cnam.sportsclub.dto.ApplicablePriceDto;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ApplicablePriceMapper {

    @Mapping(target = "amount", source = "price")
    ApplicablePriceDto toApplicablePriceDto(ApplicablePrice applicablePrice);

    @InheritInverseConfiguration
    ApplicablePrice toApplicablePrice(ApplicablePriceDto applicablePriceDto);

}
