package me.fthouraud.cnam.sportsclub.dto.mappers;

import me.fthouraud.cnam.sportsclub.domain.Subscription;
import me.fthouraud.cnam.sportsclub.dto.SubscriptionDto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SubscriptionMapper {

    @Mapping(target = "subscribedOn", source = "subscribedOn", dateFormat = "yyyy-MM-dd HH:mm")
    SubscriptionDto toSubscriptionDto(Subscription subscription);

}
