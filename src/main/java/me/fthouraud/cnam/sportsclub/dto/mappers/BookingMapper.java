package me.fthouraud.cnam.sportsclub.dto.mappers;

import me.fthouraud.cnam.sportsclub.domain.Booking;
import me.fthouraud.cnam.sportsclub.dto.BookingDto;
import me.fthouraud.cnam.sportsclub.dto.BookingSummaryDto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BookingMapper {

    @Mapping(target = "courtType", source = "court.type")
    BookingDto toBookingDto(Booking booking);

    @Mapping(target = "courtId", source = "court.id")
    @Mapping(target = "memberId", source = "member.id")
    @Mapping(target = "courtType", source = "court.type")
    @Mapping(target = "bookedForTime", source = "bookedFor", dateFormat = "HH")
    @Mapping(target = "bookedForDate", source = "bookedFor", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "memberName", expression = "java( booking.getMember().getFullName() )")
    BookingSummaryDto toBookingSummaryDto(Booking booking);

}
