package me.fthouraud.cnam.sportsclub.dto;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ApplicablePriceDto {

    private SubscriptionType subscriptionType;
    private CourtType courtType;
    private BigDecimal amount;

}
