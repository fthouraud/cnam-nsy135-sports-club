package me.fthouraud.cnam.sportsclub.dto.mappers;

import me.fthouraud.cnam.sportsclub.domain.ApplicablePrice;
import me.fthouraud.cnam.sportsclub.domain.Member;
import me.fthouraud.cnam.sportsclub.domain.Subscription;
import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.dto.AddMemberRequestDto;
import me.fthouraud.cnam.sportsclub.dto.MemberDetailsDto;
import me.fthouraud.cnam.sportsclub.dto.MemberItemDto;
import me.fthouraud.cnam.sportsclub.dto.MemberSummaryDto;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {BookingMapper.class, SubscriptionMapper.class})
public interface MemberMapper {

    @Mapping(target = "bookingCount", expression = "java( member.getBookings().size() )")
    @Mapping(target = "hasActiveSubscription", expression = "java( member.getActiveSubscription().isPresent() )")
    MemberSummaryDto toMemberSummary(Member member);

    @Mapping(target = "applicablePrices", ignore = true)
    @Mapping(target = "fullName", expression = "java( member.getFullName() )")
    @Mapping(target = "memberSummary", expression = "java( toMemberSummary(member) )")
    MemberDetailsDto toMemberDetailsDto(Member member);

    @Mapping(target = "bookings", ignore = true)
    @Mapping(target = "subscriptions", ignore = true)
    Member toMember(AddMemberRequestDto addMemberRequest);

    @Mapping(target = "fullName", expression = "java( member.getFullName() )")
    MemberItemDto toMemberItemDto(Member member);

    @AfterMapping
    default void mapApplicablePrices(@MappingTarget MemberDetailsDto memberDetails, Member member) {
        final Map<CourtType, BigDecimal> applicablePrices = member.getActiveSubscription()
                .map(Subscription::getApplicablePrices)
                .orElse(Collections.emptyList())
                .stream()
                .collect(Collectors.toMap(ApplicablePrice::getCourtType, ApplicablePrice::getPrice));
        memberDetails.setApplicablePrices(applicablePrices);
    }

}
