package me.fthouraud.cnam.sportsclub.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.YearMonth;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingFilterDto {

    private int year;
    private int month;

    public static BookingFilterDto from(YearMonth yearMonth) {
        return new BookingFilterDto(yearMonth.getYear(), yearMonth.getMonthValue());
    }

    public static BookingFilterDto ofNow() {
        final YearMonth yearMonth = YearMonth.now();
        return new BookingFilterDto(yearMonth.getYear(), yearMonth.getMonthValue());
    }

    public YearMonth toYearMonth() {
        return YearMonth.of(year, month);
    }

}
