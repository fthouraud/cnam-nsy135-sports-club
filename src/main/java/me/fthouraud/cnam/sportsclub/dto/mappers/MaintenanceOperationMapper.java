package me.fthouraud.cnam.sportsclub.dto.mappers;

import me.fthouraud.cnam.sportsclub.domain.MaintenanceOperation;
import me.fthouraud.cnam.sportsclub.dto.MaintenanceOperationDto;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MaintenanceOperationMapper {

    MaintenanceOperationDto toMaintenanceOperationDto(MaintenanceOperation maintenanceOperation);

}
