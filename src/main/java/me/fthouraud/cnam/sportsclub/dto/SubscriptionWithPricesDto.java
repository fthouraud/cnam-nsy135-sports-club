package me.fthouraud.cnam.sportsclub.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Delegate;

@Getter
@Setter
public class SubscriptionWithPricesDto {

    @Delegate
    private SubscriptionDto subscription;

}
