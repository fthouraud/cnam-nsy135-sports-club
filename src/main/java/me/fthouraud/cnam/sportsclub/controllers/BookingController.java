package me.fthouraud.cnam.sportsclub.controllers;

import me.fthouraud.cnam.sportsclub.dto.BookingFilterDto;
import me.fthouraud.cnam.sportsclub.dto.BookingRequestDto;
import me.fthouraud.cnam.sportsclub.services.BookingService;
import me.fthouraud.cnam.sportsclub.services.MemberService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/bookings")
public class BookingController extends AbstractController {

    public static final String BOOKINGS_LIST_FRAGMENT = "fragments/bookings-list";

    private final BookingService bookingService;
    private final MemberService memberService;

    @GetMapping
    public String getAllBookings(Model model) {
        return getAllFilteredBookings(BookingFilterDto.ofNow(), model);
    }

    @PostMapping
    public String newBooking(@ModelAttribute BookingRequestDto bookingRequest, Model model) {
        bookingService.registerNewBooking(bookingRequest);

        return getAllFilteredBookings(BookingFilterDto.ofNow(), model);
    }

    @PostMapping("/filter")
    public String getAllFilteredBookings(@ModelAttribute BookingFilterDto bookingFilter, Model model) {
        model.addAttribute("bookingRequest", new BookingRequestDto());
        model.addAttribute("members", memberService.getAllMemberItems());
        model.addAttribute("bookingYears", bookingService.getAllBookingsYears());
        model.addAttribute("bookingFilter", bookingFilter);

        model.addAttribute("todayBookings", bookingService.getAllBookingsOfTheDay());
        model.addAttribute("tomorrowBookings", bookingService.getAllFutureBookings());
        model.addAttribute("passedBookings", bookingService.getAllPastBookings(bookingFilter));

        return BOOKINGS_LIST_FRAGMENT;
    }

    @Override
    protected String getModuleAttribute() {
        return "bookings";
    }

}
