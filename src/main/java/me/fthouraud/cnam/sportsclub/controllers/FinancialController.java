package me.fthouraud.cnam.sportsclub.controllers;

import me.fthouraud.cnam.sportsclub.dto.FinancialFilterDto;
import me.fthouraud.cnam.sportsclub.services.BookingService;
import me.fthouraud.cnam.sportsclub.services.FinancialService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.Year;

@RequiredArgsConstructor
@Controller
@RequestMapping("/financial")
public class FinancialController extends AbstractController {

    private final FinancialService financialService;
    private final BookingService bookingService;

    @GetMapping
    public String getFinancialReport(Model model) {
        return getFinancialReportOfYear(FinancialFilterDto.builder().year(Year.now().getValue()).build(), model);
    }

    @PostMapping
    public String getFinancialReportOfYear(@ModelAttribute FinancialFilterDto financialFilter, Model model) {
        model.addAttribute("bookingYears", bookingService.getAllBookingsYears());
        model.addAttribute("financialFilter", financialFilter);
        model.addAttribute("financialInformation", financialService.getFinancialInformation(financialFilter));

        return "fragments/financial-report";
    }

    @Override
    protected String getModuleAttribute() {
        return "financial";
    }

}
