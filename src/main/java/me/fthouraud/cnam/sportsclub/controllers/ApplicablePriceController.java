package me.fthouraud.cnam.sportsclub.controllers;

import me.fthouraud.cnam.sportsclub.dto.ApplicablePriceDto;
import me.fthouraud.cnam.sportsclub.services.ApplicablePriceService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/applicable-prices")
public class ApplicablePriceController extends AbstractController {

    private final ApplicablePriceService applicablePriceService;

    @GetMapping
    public String getApplicablePrices(Model model) {
        model.addAttribute("applicablePrices", applicablePriceService.getApplicablePrices());
        model.addAttribute("applicablePrice", new ApplicablePriceDto());
        return "fragments/applicable-prices-list";
    }

    @PostMapping
    public String updateApplicablePrice(@ModelAttribute ApplicablePriceDto applicablePrice, Model model) {
        applicablePriceService.updateApplicablePrice(applicablePrice);
        return getApplicablePrices(model);
    }

    @Override
    protected String getModuleAttribute() {
        return "applicable-prices";
    }

}
