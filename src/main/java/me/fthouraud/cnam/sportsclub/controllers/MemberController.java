package me.fthouraud.cnam.sportsclub.controllers;

import me.fthouraud.cnam.sportsclub.dto.AddMemberRequestDto;
import me.fthouraud.cnam.sportsclub.services.MemberService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@Controller
@RequestMapping("/members")
public class MemberController extends AbstractController {

    private final MemberService memberService;

    @GetMapping
    public String getMembersList(Model model) {
        model.addAttribute("members", memberService.getAllMembers());
        model.addAttribute("member", new AddMemberRequestDto());
        return "fragments/members-list";
    }

    @PostMapping
    public String addMember(@ModelAttribute("member") AddMemberRequestDto addMemberRequestDto, Model model) {
        memberService.addMember(addMemberRequestDto);
        return getMembersList(model);
    }

    @GetMapping("/{memberId}")
    public String getMemberDetails(@PathVariable("memberId") Long memberId, Model model) {
        model.addAttribute("member", memberService.getMemberDetails(memberId));
        return "fragments/member-details";
    }

    @Override
    protected String getModuleAttribute() {
        return "members";
    }

}
