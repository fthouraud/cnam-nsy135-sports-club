package me.fthouraud.cnam.sportsclub.controllers;

import me.fthouraud.cnam.sportsclub.services.CourtService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/courts")
public class CourtController extends AbstractController {

    private final CourtService courtService;

    @GetMapping
    public String getCourtsList(Model model) {
        model.addAttribute("courts", courtService.getAllCourts());
        return "fragments/courts-list";
    }

    @GetMapping("/{courtId}")
    public String getCourt(@PathVariable("courtId") Long courtId, Model model) {
        model.addAttribute("court", courtService.getCourt(courtId));
        return "fragments/court-details";
    }

    @Override
    protected String getModuleAttribute() {
        return "courts";
    }

}
