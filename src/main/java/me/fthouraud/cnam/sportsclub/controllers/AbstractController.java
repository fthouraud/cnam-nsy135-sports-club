package me.fthouraud.cnam.sportsclub.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

public abstract class AbstractController {

    private static final String MODULE_ATTRIBUTE = "module";

    @ModelAttribute
    public void addModuleAttribute(Model model) {
        model.addAttribute(MODULE_ATTRIBUTE, getModuleAttribute());
    }

    protected abstract String getModuleAttribute();

}
