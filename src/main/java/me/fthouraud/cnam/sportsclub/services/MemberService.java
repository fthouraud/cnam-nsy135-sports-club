package me.fthouraud.cnam.sportsclub.services;

import me.fthouraud.cnam.sportsclub.domain.ApplicablePrice;
import me.fthouraud.cnam.sportsclub.domain.Member;
import me.fthouraud.cnam.sportsclub.domain.Subscription;
import me.fthouraud.cnam.sportsclub.dto.AddMemberRequestDto;
import me.fthouraud.cnam.sportsclub.dto.MemberDetailsDto;
import me.fthouraud.cnam.sportsclub.dto.MemberItemDto;
import me.fthouraud.cnam.sportsclub.dto.MemberSummaryDto;
import me.fthouraud.cnam.sportsclub.dto.mappers.MemberMapper;
import me.fthouraud.cnam.sportsclub.repositories.ApplicablePriceRepository;
import me.fthouraud.cnam.sportsclub.repositories.MemberRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Service
public class MemberService {

    private final MemberRepository memberRepository;
    private final ApplicablePriceRepository applicablePriceRepository;
    private final MemberMapper memberMapper;

    public List<MemberSummaryDto> getAllMembers() {
        return StreamSupport.stream(this.memberRepository.findAll().spliterator(), false)
                .map(memberMapper::toMemberSummary)
                .collect(Collectors.toList());
    }

    public MemberDetailsDto getMemberDetails(Long memberId) {
        return this.memberMapper.toMemberDetailsDto(this.memberRepository.findOne(memberId));
    }

    @Transactional
    public void addMember(AddMemberRequestDto addMemberRequest) {
        final Member newMember = memberMapper.toMember(addMemberRequest);

        final List<ApplicablePrice> applicablePrices = applicablePriceRepository
                .findAllBySubscriptionType(addMemberRequest.getSubscriptionType());

        final Optional<ApplicablePrice> subscriptionApplicablePrice = applicablePrices.stream()
                .filter(applicablePrice -> applicablePrice.getCourtType() == null)
                .findFirst();
        subscriptionApplicablePrice.ifPresent(applicablePrices::remove);

        final Subscription subscription = Subscription.builder()
                .amount(subscriptionApplicablePrice.map(ApplicablePrice::getPrice).orElse(BigDecimal.ZERO))
                .type(addMemberRequest.getSubscriptionType())
                .applicablePrices(applicablePrices)
                .subscribedOn(LocalDateTime.now())
                .member(newMember)
                .build();
        newMember.setSubscriptions(Collections.singletonList(subscription));
        newMember.setBookings(Collections.emptyList());

        memberRepository.save(newMember);
    }

    public List<MemberItemDto> getAllMemberItems() {
        return StreamSupport.stream(memberRepository.findAll().spliterator(), false)
                .map(memberMapper::toMemberItemDto)
                .collect(Collectors.toList());
    }

}
