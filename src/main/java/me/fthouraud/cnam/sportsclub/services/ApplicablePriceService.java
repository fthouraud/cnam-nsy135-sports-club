package me.fthouraud.cnam.sportsclub.services;

import me.fthouraud.cnam.sportsclub.domain.ApplicablePrice;
import me.fthouraud.cnam.sportsclub.dto.ApplicablePriceDto;
import me.fthouraud.cnam.sportsclub.dto.mappers.ApplicablePriceMapper;
import me.fthouraud.cnam.sportsclub.repositories.ApplicablePriceRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ApplicablePriceService {

    private final ApplicablePriceRepository applicablePriceRepository;
    private final ApplicablePriceMapper applicablePriceMapper;

    public List<ApplicablePriceDto> getApplicablePrices() {
        return applicablePriceRepository.findAllByActiveIsTrue().stream()
                .map(applicablePriceMapper::toApplicablePriceDto)
                .sorted(Comparator.comparing(ApplicablePriceDto::getSubscriptionType))
                .collect(Collectors.toList());
    }

    @Transactional
    public void updateApplicablePrice(ApplicablePriceDto applicablePrice) {
        final Optional<ApplicablePrice> actualPriceOptional =
                applicablePriceRepository.findByCourtTypeAndSubscriptionTypeAndActiveIsTrue(applicablePrice.getCourtType(),
                        applicablePrice.getSubscriptionType());

        actualPriceOptional.ifPresent(actualPrice -> {
            actualPrice.setActive(false);
            applicablePriceRepository.save(actualPrice);
        });

        final ApplicablePrice newPrice = applicablePriceMapper.toApplicablePrice(applicablePrice);
        newPrice.setActive(true);
        applicablePriceRepository.save(newPrice);
    }

}
