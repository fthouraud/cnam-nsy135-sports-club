package me.fthouraud.cnam.sportsclub.services;

import me.fthouraud.cnam.sportsclub.domain.Booking;
import me.fthouraud.cnam.sportsclub.domain.Member;
import me.fthouraud.cnam.sportsclub.domain.Subscription;
import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;
import me.fthouraud.cnam.sportsclub.dto.FinancialFilterDto;
import me.fthouraud.cnam.sportsclub.dto.FinancialInformationDto;
import me.fthouraud.cnam.sportsclub.repositories.BookingRepository;
import me.fthouraud.cnam.sportsclub.repositories.MaintenanceOperationRepository;
import me.fthouraud.cnam.sportsclub.repositories.MemberRepository;
import me.fthouraud.cnam.sportsclub.repositories.SubscriptionRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Service
public class FinancialService {

    private final MemberRepository memberRepository;
    private final BookingRepository bookingRepository;
    private final MaintenanceOperationRepository maintenanceOperationRepository;
    private final SubscriptionRepository subscriptionRepository;

    public FinancialInformationDto getFinancialInformation(FinancialFilterDto financialFilter) {
        final Iterable<Booking> bookings = bookingRepository.findAll();

        return FinancialInformationDto.builder()
                .yearlyIncomePerMonths(getYearlyIncomeByMonths(bookings))
                .incomeOfTheYear(getIncomeOfTheYear(financialFilter.getYear()))
                .yearlyIncomePerCourtTypes(getYearlyIncomeByCourtTypes(bookings))
                .yearlyIncomePerMembers(getYearlyIncomePerMembers(financialFilter.getYear()))
                .yearlySubscriptionsBySubscriptionTypes(getYearlySubscriptionsBySubscriptionTypes(subscriptionRepository.findAll()))
                .maintenanceCostOfTheYear(maintenanceOperationRepository.findMaintenanceOperationCostOfAYear(financialFilter.getYear()))
                .build();
    }

    private Map<String, BigDecimal> getYearlyIncomePerMembers(int year) {
        return StreamSupport.stream(memberRepository.findAll().spliterator(), false)
                .sorted(Comparator.comparing(Member::getFullName))
                .collect(Collectors.toMap(
                        Member::getFullName,
                        member -> member.getBookings().stream()
                                .filter(booking -> booking.getBookedFor().getYear() == year)
                                .map(Booking::getBilledAmount)
                                .reduce(BigDecimal.ZERO, BigDecimal::add)
                ));
    }

    private Map<Integer, Map<Integer, BigDecimal>> getYearlyIncomeByMonths(Iterable<Booking> bookings) {
        return StreamSupport.stream(bookings.spliterator(), false)
                .sorted(Comparator.comparing(Booking::getBookedFor))
                .collect(Collectors.groupingBy(
                        booking -> booking.getBookedFor().getYear(),
                        Collectors.groupingBy(booking -> booking.getBookedFor().getMonthValue(), collectToBilledAmountSum())
                ));
    }

    private Map<Integer, Map<CourtType, BigDecimal>> getYearlyIncomeByCourtTypes(Iterable<Booking> bookings) {
        return StreamSupport.stream(bookings.spliterator(), false)
                .sorted(Comparator.comparing(Booking::getBookedFor))
                .collect(Collectors.groupingBy(
                        booking -> booking.getBookedFor().getYear(),
                        Collectors.groupingBy(booking -> booking.getCourt().getType(), collectToBilledAmountSum())
                ));
    }

    private Map<Integer, Map<SubscriptionType, Long>> getYearlySubscriptionsBySubscriptionTypes(Iterable<Subscription> subscriptions) {
        return StreamSupport.stream(subscriptions.spliterator(), false)
                .sorted(Comparator.comparing(Subscription::getSubscribedOn))
                .collect(Collectors.groupingBy(
                        subscription -> subscription.getSubscribedOn().getYear(),
                        Collectors.groupingBy(Subscription::getType, Collectors.counting())
                ));
    }

    private BigDecimal getIncomeOfTheYear(int year) {
        final BigDecimal billedAmount = bookingRepository.findBilledAmountForAYear(year);
        final BigDecimal subscriptionAmount = subscriptionRepository.findSubscriptionAmountForAYear(year);

        return billedAmount.add(subscriptionAmount);
    }

    private Collector<Booking, ?, BigDecimal> collectToBilledAmountSum() {
        return Collectors.mapping(Booking::getBilledAmount, Collectors.reducing(BigDecimal.ZERO, BigDecimal::add));
    }

}
