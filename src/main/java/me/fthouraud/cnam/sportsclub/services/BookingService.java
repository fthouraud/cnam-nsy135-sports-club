package me.fthouraud.cnam.sportsclub.services;

import me.fthouraud.cnam.sportsclub.domain.Booking;
import me.fthouraud.cnam.sportsclub.dto.BookingFilterDto;
import me.fthouraud.cnam.sportsclub.dto.BookingRequestDto;
import me.fthouraud.cnam.sportsclub.dto.BookingSummaryDto;
import me.fthouraud.cnam.sportsclub.dto.mappers.BookingMapper;
import me.fthouraud.cnam.sportsclub.repositories.BookingRepository;
import me.fthouraud.cnam.sportsclub.repositories.CourtRepository;
import me.fthouraud.cnam.sportsclub.repositories.MemberRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final MemberRepository memberRepository;
    private final CourtRepository courtRepository;
    private final BookingMapper bookingMapper;

    private static LocalDateTime getTodayAtMidnight() {
        return LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
    }

    private static LocalDateTime getLocalDateAtMidnight(LocalDate localDate) {
        return LocalDateTime.of(localDate, LocalTime.of(0, 0));
    }

    public List<BookingSummaryDto> getAllBookingsOfTheDay() {
        final LocalDateTime todayAtMidnight = getTodayAtMidnight();
        return mapAndCollect(
                bookingRepository.findAllByBookedForIsBetweenOrderByBookedFor(todayAtMidnight, todayAtMidnight.plusHours(23)).stream()
        );
    }

    public List<BookingSummaryDto> getAllFutureBookings() {
        return mapAndCollect(bookingRepository.findAllByBookedForIsAfterOrderByBookedFor(getTodayAtMidnight().plusDays(1)).stream());
    }

    public List<BookingSummaryDto> getAllPastBookings(BookingFilterDto bookingFilter) {
        final LocalDateTime yearMonthStart = Optional.ofNullable(bookingFilter)
                .map(BookingFilterDto::toYearMonth)
                .map(yearMonth -> LocalDateTime.of(yearMonth.getYear(), yearMonth.getMonthValue(), 1, 0, 0))
                .orElse(getLocalDateAtMidnight(LocalDate.now().withDayOfMonth(1)));

        final LocalDateTime yearMonthEnd = yearMonthStart.plusMonths(1).withDayOfMonth(1).minusDays(1);

        return mapAndCollect(bookingRepository.findAllByBookedForIsBetweenOrderByBookedFor(yearMonthStart, yearMonthEnd).stream());
    }

    public List<Integer> getAllBookingsYears() {
        return bookingRepository.findAllBookingYears();
    }

    public void registerNewBooking(BookingRequestDto bookingRequest) {
        final Booking booking = Booking.builder()
                .member(memberRepository.findOne(bookingRequest.getMemberId()))
                .bookedFor(LocalDateTime.of(
                        LocalDate.parse(bookingRequest.getBookedForDate()),
                        LocalTime.parse(bookingRequest.getBookedForTime()).withMinute(0).withSecond(0).withNano(0)
                ))
                .bookedOn(LocalDateTime.now())
                .build();

        courtRepository.findByType(bookingRequest.getCourtType()).stream().findAny().ifPresent(booking::setCourt);

        booking.getMember().getActiveSubscription().ifPresent(subscription -> subscription.getApplicablePrices().stream()
                .filter(applicablePrice -> applicablePrice.getCourtType() == bookingRequest.getCourtType())
                .findFirst()
                .ifPresent(applicablePrice -> booking.setBilledAmount(applicablePrice.getPrice()))
        );

        bookingRepository.save(booking);
    }

    private List<BookingSummaryDto> mapAndCollect(Stream<Booking> bookingsStream) {
        return bookingsStream.map(bookingMapper::toBookingSummaryDto)
                .collect(Collectors.toList());
    }

}
