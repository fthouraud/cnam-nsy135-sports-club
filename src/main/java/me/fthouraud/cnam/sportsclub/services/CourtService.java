package me.fthouraud.cnam.sportsclub.services;

import me.fthouraud.cnam.sportsclub.dto.CourtDetailDto;
import me.fthouraud.cnam.sportsclub.dto.CourtSummaryDto;
import me.fthouraud.cnam.sportsclub.dto.mappers.CourtMapper;
import me.fthouraud.cnam.sportsclub.repositories.CourtRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor
@Service
public class CourtService {

    private final CourtRepository courtRepository;
    private final CourtMapper courtMapper;

    public List<CourtSummaryDto> getAllCourts() {
        return StreamSupport.stream(courtRepository.findAll().spliterator(), false)
                .map(courtMapper::toCourtDto)
                .collect(Collectors.toList());
    }

    public CourtDetailDto getCourt(Long courtId) {
        return courtMapper.toCourtDetailDto(courtRepository.findOne(courtId));
    }

}
