package me.fthouraud.cnam.sportsclub.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.YearMonth;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Booking extends IdentifiableEntity {

    @ManyToOne(optional = false)
    private Member member;

    @ManyToOne(optional = false)
    private Court court;

    @Column(nullable = false, updatable = false)
    private LocalDateTime bookedOn;

    @Column(nullable = false, updatable = false)
    private LocalDateTime bookedFor;

    @Column(nullable = false, updatable = false, precision = 5, scale = 2)
    private BigDecimal billedAmount;

    @Column(updatable = false)
    private LocalDateTime cancelledOn;

    /**
     * Indicate if this {@link Booking} is from last month.
     * The result is based on the field {@link Booking#bookedFor}.
     *
     * @return {@code true} when it has been booked for last month; {@code false} otherwise.
     */
    public boolean isFromLastMonth() {
        return YearMonth.now().minusMonths(1).equals(YearMonth.from(bookedFor));
    }

    public boolean isCancelled() {
        return cancelledOn != null;
    }

}
