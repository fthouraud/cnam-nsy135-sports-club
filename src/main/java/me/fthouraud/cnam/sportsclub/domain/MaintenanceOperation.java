package me.fthouraud.cnam.sportsclub.domain;

import me.fthouraud.cnam.sportsclub.domain.enumeration.OperationType;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class MaintenanceOperation extends IdentifiableEntity {

    @ManyToOne(optional = false)
    private Court court;

    @Enumerated
    @Column(nullable = false, updatable = false)
    private OperationType type;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false, updatable = false)
    private LocalDate maintenanceDate;

    @Column(nullable = false, updatable = false, precision = 12, scale = 2)
    private BigDecimal cost;

}
