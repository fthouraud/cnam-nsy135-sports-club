package me.fthouraud.cnam.sportsclub.domain.enumeration;

public enum OperationType {
    EQUIPMENT_PURCHASE,
    FLOOR_RENOVATION,
    OTHER_RENOVATION
}
