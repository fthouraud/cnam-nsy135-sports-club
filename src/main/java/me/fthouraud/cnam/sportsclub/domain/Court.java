package me.fthouraud.cnam.sportsclub.domain;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@Inheritance
public class Court extends IdentifiableEntity {

    @Enumerated
    @Column(nullable = false, updatable = false, insertable = false)
    private CourtType type;

    @Column(nullable = false, updatable = false)
    private LocalDate commissioningDate;

    @Column(nullable = false, precision = 1)
    private int rating;

    @OneToMany(mappedBy = "court")
    private List<MaintenanceOperation> maintenanceOperations;

}
