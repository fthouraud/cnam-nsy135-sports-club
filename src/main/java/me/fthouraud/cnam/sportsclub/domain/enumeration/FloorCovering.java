package me.fthouraud.cnam.sportsclub.domain.enumeration;

public enum FloorCovering {
    CLAY,
    HARD
}
