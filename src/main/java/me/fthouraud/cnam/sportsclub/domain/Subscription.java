package me.fthouraud.cnam.sportsclub.domain;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;
import me.fthouraud.cnam.sportsclub.utils.CommonUtils;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Subscription extends IdentifiableEntity {

    @Enumerated
    @Column(nullable = false, updatable = false)
    private SubscriptionType type;

    @ManyToOne(optional = false)
    private Member member;

    @Column(nullable = false, updatable = false)
    private LocalDateTime subscribedOn;

    @Column(nullable = false, updatable = false)
    private BigDecimal amount;

    @ManyToMany
    @JoinTable(name = "subscription_applicable_price",
            joinColumns = @JoinColumn(name = "subscription_id"),
            inverseJoinColumns = @JoinColumn(name = "applicable_price_id"))
    private List<ApplicablePrice> applicablePrices;

    /**
     * Indicate if this {@link Subscription} is active or not.
     * A subscription is active when its subscription date isn't over a year.
     *
     * @return {@code true} this subscription as less than a year of existence; {@code false} otherwise.
     */
    public boolean isActive() {
        return subscribedOn.until(LocalDateTime.now(), ChronoUnit.YEARS) < 1;
    }

    /**
     * Get the amount of this {@link Subscription} as a monthly amount.
     *
     * @return the amount value divided by 12.
     */
    public BigDecimal getMonthlyAmount() {
        return Optional.ofNullable(amount).orElse(BigDecimal.ZERO).divide(BigDecimal.valueOf(12), RoundingMode.HALF_EVEN);
    }

    /**
     * Get the price for a court based on its {@link CourtType}.
     * The price is the value of {@link ApplicablePrice#getPrice()}.
     *
     * @param courtType looked for type of court
     *
     * @return if found, the applicable price; {@code 0} otherwise
     */
    public BigDecimal getPriceByCourtType(CourtType courtType) {
        return CommonUtils.ofNullableList(applicablePrices).stream()
                .filter(applicablePrice -> applicablePrice.getCourtType() == courtType)
                .map(ApplicablePrice::getPrice)
                .findFirst()
                .orElse(BigDecimal.ZERO);
    }

}
