package me.fthouraud.cnam.sportsclub.domain;

import me.fthouraud.cnam.sportsclub.utils.CommonUtils;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Entity
public class Member extends IdentifiableEntity {

    @Column(nullable = false, length = 50)
    private String firstName;

    @Column(nullable = false, length = 50)
    private String lastName;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false)
    private String email;

    @ManyToMany(mappedBy = "member")
    private List<Booking> bookings;

    @ManyToMany(mappedBy = "member", cascade = {CascadeType.PERSIST})
    private List<Subscription> subscriptions;

    public String getFullName() {
        return String.format("%s %s", getFirstName(), getLastName());
    }

    /**
     * Get the active {@link Subscription} of this {@link Member}.
     * A subscription is active only when the time between the subscription date and today is less than a year.
     *
     * @return an optional wrapping the active subscription if any; an empty optional otherwise.
     */
    public Optional<Subscription> getActiveSubscription() {
        return CommonUtils.ofNullableList(subscriptions).stream()
                .filter(Subscription::isActive)
                .findFirst();
    }

    /**
     * Get the last {@link Subscription} of this {@link Member}.
     *
     * @return an optional wrapping the last subscription if any; an empty optional otherwise.
     */
    public Optional<Subscription> getLastSubscription() {
        return CommonUtils.ofNullableList(subscriptions).stream()
                .max(Comparator.comparing(Subscription::getSubscribedOn));
    }

    /**
     * Figure out the last month booking's billed amount of this {@link Member}.
     *
     * @return the billed amount for the last month if this member has any booking; 0 otherwise.
     */
    public BigDecimal getLastMonthBilledAmount() {
        final BigDecimal subscriptionMonthlyAmount = getLastSubscription().map(Subscription::getMonthlyAmount).orElse(BigDecimal.ZERO);
        return CommonUtils.ofNullableList(bookings).stream()
                .filter(Booking::isFromLastMonth)
                .map(Booking::getBilledAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .add(subscriptionMonthlyAmount);
    }

}
