package me.fthouraud.cnam.sportsclub.domain;

import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class ApplicablePrice {

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated
    @Column(nullable = false, updatable = false)
    private SubscriptionType subscriptionType;

    @Enumerated
    @Column(nullable = false, updatable = false)
    private CourtType courtType;

    @Column(nullable = false, precision = 5, scale = 2)
    private BigDecimal price;

    @Column(nullable = false)
    private boolean active;

}
