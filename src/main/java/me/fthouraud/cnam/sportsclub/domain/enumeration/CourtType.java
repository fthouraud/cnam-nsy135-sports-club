package me.fthouraud.cnam.sportsclub.domain.enumeration;

public enum CourtType {
    BADMINTON,
    TENNIS,
    SQUASH
}
