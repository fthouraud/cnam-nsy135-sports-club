package me.fthouraud.cnam.sportsclub.domain.enumeration;

public enum SubscriptionType {
    PLAN,
    TICKET
}
