package me.fthouraud.cnam.sportsclub.repositories;

import me.fthouraud.cnam.sportsclub.domain.Subscription;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {

    @Query("SELECT SUM(s.amount) FROM Subscription s WHERE YEAR(s.subscribedOn) = :year")
    BigDecimal findSubscriptionAmountForAYear(@Param("year") int year);

}
