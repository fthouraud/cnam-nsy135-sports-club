package me.fthouraud.cnam.sportsclub.repositories;

import me.fthouraud.cnam.sportsclub.domain.ApplicablePrice;
import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;
import me.fthouraud.cnam.sportsclub.domain.enumeration.SubscriptionType;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ApplicablePriceRepository extends CrudRepository<ApplicablePrice, Long> {

    Optional<ApplicablePrice> findByCourtTypeAndSubscriptionTypeAndActiveIsTrue(CourtType courtType, SubscriptionType subscriptionType);

    List<ApplicablePrice> findAllByActiveIsTrue();

    List<ApplicablePrice> findAllBySubscriptionType(SubscriptionType subscriptionType);

}
