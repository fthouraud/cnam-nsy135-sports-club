package me.fthouraud.cnam.sportsclub.repositories;

import me.fthouraud.cnam.sportsclub.domain.MaintenanceOperation;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface MaintenanceOperationRepository extends CrudRepository<MaintenanceOperation, Long> {

    List<MaintenanceOperation> findAllByMaintenanceDateBetweenOrderByMaintenanceDate(LocalDate from, LocalDate to);

    @Query("SELECT sum(mo.cost) FROM MaintenanceOperation mo WHERE YEAR(mo.maintenanceDate) = :year")
    BigDecimal findMaintenanceOperationCostOfAYear(@Param("year") int year);

}
