package me.fthouraud.cnam.sportsclub.repositories;

import me.fthouraud.cnam.sportsclub.domain.Court;
import me.fthouraud.cnam.sportsclub.domain.enumeration.CourtType;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CourtRepository extends PagingAndSortingRepository<Court, Long> {

    List<Court> findByType(CourtType courtType);

}
