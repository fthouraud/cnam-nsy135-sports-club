package me.fthouraud.cnam.sportsclub.repositories;

import me.fthouraud.cnam.sportsclub.domain.Member;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Map;

@Repository
public interface MemberRepository extends CrudRepository<Member, Long> {

    @Query("SELECT DISTINCT new map(CONCAT(m.firstName, ' ', m.lastName), SUM(b.billedAmount)) " +
            "FROM Member m JOIN m.bookings b " +
            "WHERE YEAR(b.bookedFor) = :year " +
            "GROUP BY CONCAT(m.firstName, ' ', m.lastName)"
    )
    Map<String, BigDecimal> findIncomeByMemberAndForAYear(@Param("year") int year);

}
