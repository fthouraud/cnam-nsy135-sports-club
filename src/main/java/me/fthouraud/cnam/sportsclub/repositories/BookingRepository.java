package me.fthouraud.cnam.sportsclub.repositories;

import me.fthouraud.cnam.sportsclub.domain.Booking;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface BookingRepository extends PagingAndSortingRepository<Booking, Long> {

    List<Booking> findAllByBookedForIsAfterOrderByBookedFor(LocalDateTime afterDate);
    List<Booking> findAllByBookedForIsBetweenOrderByBookedFor(LocalDateTime from, LocalDateTime to);

    @Query(value = "SELECT DISTINCT YEAR(b.booked_for) AS year FROM booking AS b", nativeQuery = true)
    List<Integer> findAllBookingYears();

    @Query("SELECT SUM(b.billedAmount) FROM Booking b WHERE YEAR(b.bookedFor) = :year")
    BigDecimal findBilledAmountForAYear(@Param("year") int year);

}
