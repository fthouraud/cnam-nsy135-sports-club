package me.fthouraud.cnam.sportsclub.utils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class CommonUtils {

    private CommonUtils() {
    }

    public static <T> List<T> ofNullableList(List<T> nullableList) {
        return Optional.ofNullable(nullableList).orElse(Collections.emptyList());
    }

}
