-- MEMBERS
INSERT INTO member (id, first_name, last_name, birth_date, email) VALUES
  (1, 'Caron', 'Pinkard', PARSEDATETIME('1985-02-11', 'yyyy-MM-dd'), 'adadds0@meetup.com'),
  (2, 'Bar', 'Gillbanks', PARSEDATETIME('1987-07-18', 'yyyy-MM-dd'), 'lhalladey1@europa.eu'),
  (3, 'Natasha', 'Raspel', PARSEDATETIME('1991-06-01', 'yyyy-MM-dd'), 'adeleon2@bloglines.com'),
  (4, 'Kaleena', 'Rolley', PARSEDATETIME('1990-01-25', 'yyyy-MM-dd'), 'dhacun3@cnet.com'),
  (5, 'Felic', 'Sheirlaw', PARSEDATETIME('1961-05-07', 'yyyy-MM-dd'), 'rwerrilow4@ed.gov'),
  (6, 'Amil', 'Furlow', PARSEDATETIME('1977-03-14', 'yyyy-MM-dd'), 'slangfat5@salon.com'),
  (7, 'Susie', 'Pether', PARSEDATETIME('1986-11-28', 'yyyy-MM-dd'), 'bshatliffe6@yellowbook.com');

-- SUBSCRIPTIONS
-- SUBSCRIPTION TYPE: 0 -> PLAN, 1 -> TICKET
INSERT INTO subscription (id, type, member_id, subscribed_on, amount) VALUES
  (1, 0, 1, PARSEDATETIME('2017-01-02 15:29', 'yyyy-MM-dd HH:mm'), 200.00),
  (2, 1, 2, PARSEDATETIME('2017-01-02 18:02', 'yyyy-MM-dd HH:mm'), 0.00),
  (3, 0, 3, PARSEDATETIME('2017-01-18 13:18', 'yyyy-MM-dd HH:mm'), 200.00),
  (4, 1, 4, PARSEDATETIME('2017-01-25 08:05', 'yyyy-MM-dd HH:mm'), 0.00),
  (5, 1, 5, PARSEDATETIME('2017-02-19 20:57', 'yyyy-MM-dd HH:mm'), 0.00),
  (6, 1, 1, PARSEDATETIME('2018-01-03 10:05', 'yyyy-MM-dd HH:mm'), 0.00),
  (7, 0, 2, PARSEDATETIME('2018-01-03 14:05', 'yyyy-MM-dd HH:mm'), 200.00),
  (8, 0, 6, PARSEDATETIME('2018-01-26 14:02', 'yyyy-MM-dd HH:mm'), 200.00),
  (9, 0, 3, PARSEDATETIME('2018-02-01 12:05', 'yyyy-MM-dd HH:mm'), 200.00),
  (10, 0, 7, PARSEDATETIME('2018-02-03 18:26', 'yyyy-MM-dd HH:mm'), 200.00);

-- SUBSCRIPTION APPLICABLE PRICES
-- APPLICABLE PRICES: 2, 3, 4 -> PLAN, 6, 7, 8 -> TICKET
INSERT INTO subscription_applicable_price (subscription_id, applicable_price_id) VALUES
  (1, 2), (1, 3), (1, 4),
  (2, 6), (2, 7), (2, 8),
  (3, 2), (3, 3), (3, 4),
  (4, 6), (4, 7), (4, 8),
  (5, 6), (5, 7), (5, 8),
  (6, 2), (6, 3), (6, 4),
  (7, 2), (7, 3), (7, 4),
  (8, 6), (8, 7), (8, 8),
  (9, 2), (9, 3), (9, 4),
  (10, 2), (10, 3), (10, 4);
