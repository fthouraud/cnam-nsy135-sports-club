-- SUBSCRIPTION TYPE: 0 -> PLAN, 1 -> TICKET
-- COURT TYPE: 0 -> BADMINTON, 1 -> TENNIS, 2 -> SQUASH
INSERT INTO applicable_price (id, subscription_type, court_type, price) VALUES
  (1, 0, NULL, 200.00),
  (2, 0, 0, 10.00),
  (3, 0, 1, 11.00),
  (4, 0, 2, 9.00),
  (5, 1, NULL, 0.00),
  (6, 1, 0, 20.00),
  (7, 1, 1, 22.00),
  (8, 1, 2, 18.00);
