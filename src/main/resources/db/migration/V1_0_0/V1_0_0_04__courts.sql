-- COURTS
-- COURT TYPE: 0 -> BADMINTON, 1 -> TENNIS, 2 -> SQUASH
INSERT INTO court (id, type, commissioning_date, rating) VALUES
  (1, 0, PARSEDATETIME('2017-01-07', 'yyyy-MM-dd'), 4),
  (2, 1, PARSEDATETIME('2017-01-07', 'yyyy-MM-dd'), 5),
  (3, 2, PARSEDATETIME('2017-04-13', 'yyyy-MM-dd'), 2),
  (4, 0, PARSEDATETIME('2017-10-25', 'yyyy-MM-dd'), 4),
  (5, 1, PARSEDATETIME('2018-01-18', 'yyyy-MM-dd'), 4);

-- SUBSCRIPTIONS
-- OPERATION TYPE: 0 -> EQUIPMENT_PURCHASE, 1 -> FLOOR_RENOVATION, 2 -> OTHER_RENOVATION
INSERT INTO maintenance_operation (id, type, court_id, maintenance_date, description, cost) VALUES
  (1, 0, 1, PARSEDATETIME('2017-01-05', 'yyyy-MM-dd'), 'Net and rackets buying', 385.24),
  (2, 0, 2, PARSEDATETIME('2017-01-05', 'yyyy-MM-dd'), 'Net and rackets buying', 353.64),
  (3, 0, 3, PARSEDATETIME('2017-01-05', 'yyyy-MM-dd'), 'Rackets buying', 126.74),
  (4, 0, 4, PARSEDATETIME('2017-10-20', 'yyyy-MM-dd'), 'Net and rackets buying', 346.73),
  (5, 1, 2, PARSEDATETIME('2018-02-03', 'yyyy-MM-dd'), 'First floor renovation', 1538.65);
