-- MEMBER
CREATE TABLE member (
  id         BIGINT IDENTITY PRIMARY KEY,
  first_name VARCHAR(50)  NOT NULL,
  last_name  VARCHAR(50)  NOT NULL,
  birth_date DATE         NOT NULL,
  email      VARCHAR(255) NOT NULL,
  UNIQUE (first_name, last_name, birth_date)
);

-- SUBSCRIPTION
CREATE TABLE subscription (
  id            BIGINT IDENTITY PRIMARY KEY,
  type          INTEGER       NOT NULL,
  member_id     BIGINT        NOT NULL,
  subscribed_on TIMESTAMP     NOT NULL,
  amount        NUMERIC(5, 2) NOT NULL,
  FOREIGN KEY (member_id) REFERENCES member (id)
);

-- APPLICABLE PRICE
CREATE TABLE applicable_price (
  id                BIGINT IDENTITY PRIMARY KEY,
  subscription_type INTEGER       NOT NULL,
  court_type        INTEGER,
  price             NUMERIC(5, 2) NOT NULL,
  active            BOOLEAN DEFAULT TRUE
);

-- SUBSCRIPTION APPLICABLE PRICE
CREATE TABLE subscription_applicable_price (
  subscription_id     BIGINT,
  applicable_price_id BIGINT,
  PRIMARY KEY (subscription_id, applicable_price_id),
  FOREIGN KEY (subscription_id) REFERENCES subscription (id),
  FOREIGN KEY (applicable_price_id) REFERENCES applicable_price (id)
);

-- COURT
CREATE TABLE court (
  id                 BIGINT IDENTITY PRIMARY KEY,
  type               INTEGER              NOT NULL,
  commissioning_date DATE                 NOT NULL,
  rating             INTEGER DEFAULT 5    NOT NULL,
  floor_covering     INTEGER,
  glazed             BOOLEAN DEFAULT FALSE,
);

-- BOOKING
CREATE TABLE booking (
  id            BIGINT IDENTITY PRIMARY KEY,
  member_id     BIGINT        NOT NULL,
  court_id      BIGINT        NOT NULL,
  booked_on     TIMESTAMP     NOT NULL,
  booked_for    TIMESTAMP     NOT NULL,
  billed_amount NUMERIC(5, 2) NOT NULL,
  cancelled_on  TIMESTAMP,
  FOREIGN KEY (member_id) REFERENCES member (id),
  FOREIGN KEY (court_id) REFERENCES court (id)
);

-- MAINTENANCE OPERATION
CREATE TABLE maintenance_operation (
  id               BIGINT IDENTITY PRIMARY KEY,
  court_id         BIGINT                   NOT NULL,
  type             INTEGER                  NOT NULL,
  description      VARCHAR(255)             NOT NULL,
  maintenance_date DATE                     NOT NULL,
  cost             NUMERIC(10, 2) DEFAULT 0 NOT NULL,
  FOREIGN KEY (court_id) REFERENCES court (id)
);
