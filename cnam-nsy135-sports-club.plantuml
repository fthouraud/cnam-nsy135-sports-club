@startuml

title __CNAM-NSY135-SPORTS-CLUB's Class Diagram__\n

  package me.fthouraud.cnam.sportsclub {
    package me.fthouraud.cnam.sportsclub.domain {
      abstract class IdentifiableEntity << mapped superclass >> {
        - id : Long
        __
        .. accessors ..
      }

      class ApplicablePrice << entity >> {
        - subscriptionType : SubscriptionType
        - courtType : CourtType
        - price : BigDecimal
        - active : boolean
        __
        .. accessors ..
      }

      class Booking << entity >> {
        - bookedOn : LocalDateTime
        - bookedFor : LocalDateTime
        - billedAmount : BigDecimal
        - cancelledOn : LocalDateTime
        __
        + isFromLastMonth() : boolean
        + isCancelled() : boolean
        .. accessors ..
      }

      class Court << entity >> {
        - type : CourtType
        - commissioningDate : LocalDate
        - rating : int
        - floorCovering : FloorCovering
        - glazed : boolean
        __
        .. accessors ..
      }

      class MaintenanceOperation << entity >> {
        - type : OperationType
        - description : String
        - maintenanceDate : LocalDate
        - cost : BigDecimal
        __
        .. accessors ..
      }

      class Member << entity >> {
        - firstName : String
        - lastName : String
        - birthDate : LocalDate
        - email : String
        __
        + getActiveSubscription() : Optional<Subscription>
        + getLastSubscription() : Optional<Subscription>
        + getLastMonthBilledAmount() : BigDecimal
        .. accessors ..
      }

      class Subscription << entity >> {
        - type : SubscriptionType
        - subscribedOn : LocalDateTime
        - amount : BigDecimal
        __
        + isActive()
        + getMonthlyAmount()
        .. accessors ..
      }
    }

    package me.fthouraud.cnam.sportsclub.domain {
      package me.fthouraud.cnam.sportsclub.domain.enumeration {
        enum CourtType {
          BADMINTON
          TENNIS
          SQUASH
        }

        enum FloorCovering {
          CLAY
          HARD
        }

        enum OperationType {
          EQUIPMENT_PURCHASE
          FLOOR_RENOVATION
          OTHER_RENOVATION
        }

        enum SubscriptionType {
          PLAN
          TICKET
        }
      }
    }
  }

  Member -up-|> IdentifiableEntity
  Member "1" *-- "*" Subscription : subscriptions
  Member "1" *-- "*" Booking : bookings

  Subscription -up-|> IdentifiableEntity
  Subscription o-- ApplicablePrice : applicablePrices

  Booking -up-|> IdentifiableEntity

  Court -up-|> IdentifiableEntity
  Court *-- MaintenanceOperation : maintenanceOperations
  Court <-- Booking : court

  MaintenanceOperation -up-|> IdentifiableEntity

  ApplicablePrice -up-|> IdentifiableEntity

right footer

UML class diagram for the sports club project.
Created by Fabien Thouraud.

endfooter

@enduml
